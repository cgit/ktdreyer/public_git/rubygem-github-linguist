#!/bin/bash

set -e

VERSION=2.10.9

# GitHub has not pushed a tag for 2.10.7.
# https://github.com/github/linguist/issues/869
#GITSHA=3ece15b3984191e5b0f5656173d7cd8575a2b6c7

# If you are using a public tag, then comment out the GITSHA definition above.
if [ -z "$GITSHA" ]; then
  # Upstream has pushed a public tag for $VERSION.
  GITHUBURL=https://github.com/github/linguist/archive/v${VERSION}.zip
  DIRNAME=linguist-${VERSION}
else
  # We don't have a public tag, so we have to go off a SHA instead.
  GITHUBURL=https://github.com/github/linguist/archive/${GITSHA}.zip
  DIRNAME=linguist-${GITSHA}
fi

# download zipball
if [[ ! -f github-linguist-$VERSION.zip ]]; then
    curl -o github-linguist-$VERSION.zip -L $GITHUBURL
fi

# extract zipball
[ -d $DIRNAME ] && rm -r $DIRNAME
unzip github-linguist-$VERSION.zip

pushd $DIRNAME
  # repack
  tar -cJvf github-linguist-$VERSION-tests.tar.xz test samples
  mv github-linguist-$VERSION-tests.tar.xz ..
popd

# Clean up
rm github-linguist-$VERSION.zip
rm -r $DIRNAME
