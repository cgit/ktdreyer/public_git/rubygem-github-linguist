%global gem_name github-linguist

Name: rubygem-%{gem_name}
Version: 2.10.9
Release: 1%{?dist}
Summary: GitHub Language detection
Group: Development/Languages
License: MIT
URL: https://github.com/github/linguist
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Upstream does not ship the test suite in the gem.
Source1: %{name}-generate-test-tarball.sh
Source2: %{gem_name}-%{version}-tests.tar.xz
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(charlock_holmes) => 0.6.6
Requires: rubygem(charlock_holmes) < 0.7
Requires: rubygem(escape_utils) >= 0.3.1
Requires: rubygem(mime-types) => 1.19
Requires: rubygem(mime-types) < 2
Requires: rubygem(pygments.rb) => 0.5.4
Requires: rubygem(pygments.rb) < 0.6
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(mocha)
BuildRequires: rubygem(charlock_holmes) => 0.6.6
BuildRequires: rubygem(charlock_holmes) < 0.7
BuildRequires: rubygem(escape_utils) >= 0.3.1
BuildRequires: rubygem(mime-types) => 1.19
BuildRequires: rubygem(mime-types) < 2
BuildRequires: rubygem(pygments.rb) => 0.5.4
BuildRequires: rubygem(pygments.rb) < 0.6
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Use this library to detect blob languages, highlight code, ignore binary
files, suppress generated files in diffs, and generate language breakdown
graphs.

%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n %{gem_name}-%{version} -a 2

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -pa .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%check
export LANG=en_US.UTF-8
cp -pr test .%{gem_instdir}
cp -pr samples .%{gem_instdir}
pushd .%{gem_instdir}
  ruby -Ilib test/test_*.rb
  rm -rf test samples
popd


%files
%dir %{gem_instdir}
%{_bindir}/linguist
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Feb 10 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.10.9-1
- Update to github-linguist 2.10.9

* Fri Dec 27 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.10.7-1
- Initial package
